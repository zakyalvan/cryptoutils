#README

Just a simple tools. Clone this repository (Make sure git installed) 

```sh 
git clone https://zakyalvan@bitbucket.org/zakyalvan/cryptoutils.git
```

Go to cloned project directory, then build using following command

```sh
$ ./mvnw clean install -DskipTests
```

Then, run the jar using command

```sh 
$ java -jar target/xxcryptit.jar --key.file=[/path/to/secret/key/file] --alg.name=[aes|3des] --run.mode=[encrypt|decrypt] --input.data=[plain string (encrypt)|base64 encoded string (decrypt)]
```

```--key.file```, mandatory secret key file location.

```--alg.name```, mandatory used algorithm name, either ```aes``` or ```3des```.

```--run.mode```, either ```encrypt``` or ```decrypt```.

```--input.data```, mandatory plain string for ```encrypt``` run mode and base64 encoded string for ```decrypt``` run mode.

```--block.cipher```, block cipher mode. ```ECB``` default for ```3des``` algorithm, ```CBC``` default for ```aes``` algorithm.

```--init.vector```, optional init vector value, must be in hex representation (Default value for aes/cbc mode is ```31323334353630303030303030303030```).