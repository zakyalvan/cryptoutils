package info.zakyalvan.crypto;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;

import org.apache.commons.codec.binary.Hex;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

/**
 * 
 * @author zakyalvan
 * @since 14 Jan 2017
 */
@Validated
@ConfigurationProperties(prefix="huawei.encryption.tripple-des")
public class TrippleDesCipherOperations implements CipherOperations, InitializingBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrippleDesCipherOperations.class);

	@NotNull
	private Resource secretKeyFile;

	@NotBlank
	private String blockCipherMode = "ECB";

	private String initVectorString = "";
	
	private SecretKeySpec secretKey;

	public Resource getSecretKeyFile() {
		return secretKeyFile;
	}
	public void setSecretKeyFile(Resource secretKeyFile) {
		this.secretKeyFile = secretKeyFile;
	}

	public String getBlockCipherMode() {
		return blockCipherMode;
	}
	public void setBlockCipherMode(String blockCipherMode) {
		this.blockCipherMode = blockCipherMode;
	}

	public String getInitVectorString() {
		return initVectorString;
	}
	public void setInitVectorString(String initVectorString) {
		this.initVectorString = initVectorString;
	}
	
	@Override
	public byte[] encrypt(byte[] raw) throws CipherOperationException {
		LOGGER.trace("Encrypt raw bytes using 3des chiper");
		try {
			String transformation = "DESede/" + blockCipherMode + "/PKCS5Padding";
			Cipher cipher = Cipher.getInstance(transformation);
			if(StringUtils.hasText(initVectorString)) {
				byte[] initVector = Hex.decodeHex(initVectorString.toCharArray());
				IvParameterSpec initVectorSpec = new IvParameterSpec(Arrays.copyOf(initVector, 8));
				cipher.init(Cipher.ENCRYPT_MODE, secretKey, initVectorSpec);
			}
			else {
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			}
			return cipher.doFinal(raw);
		}
		catch(Exception e) {
			throw new CipherOperationException(e);
		}
	}

	@Override
	public byte[] decrypt(byte[] encrypted) throws CipherOperationException {
		LOGGER.trace("Decrypt encrypted bytes using 3des chiper");
		try {
			String transformation = "DESede/" + blockCipherMode + "/PKCS5Padding";
			Cipher cipher = Cipher.getInstance(transformation);
			if(StringUtils.hasText(initVectorString)) {
				byte[] initVector = Hex.decodeHex(initVectorString.toCharArray());
				IvParameterSpec initVectorSpec = new IvParameterSpec(Arrays.copyOf(initVector, 8));
				cipher.init(Cipher.DECRYPT_MODE, secretKey, initVectorSpec);
			}
			else {
				cipher.init(Cipher.DECRYPT_MODE, secretKey);
			}
			return cipher.doFinal(encrypted);
		}
		catch (Exception e) {
			throw new CipherOperationException(e);
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		LOGGER.debug("Validate key and initialize secret key");
		Assert.isTrue(secretKeyFile.exists(), "Given key file is not found");

		try(InputStream keyStream = new BufferedInputStream(secretKeyFile.getInputStream())) {
			byte[] keyBytes = new byte[(int) (Files.size(secretKeyFile.getFile().toPath()))];
			keyStream.read(keyBytes);
			secretKey = new SecretKeySpec(keyBytes, "DESede");
		}
	}
}
