package info.zakyalvan.crypto;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;

import org.apache.commons.codec.binary.Hex;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

/**
 * @author zakyalvan
 * @since 13 Jan 2017
 */
@Validated
@ConfigurationProperties(prefix = "huawei.encryption.aes")
public class AESCipherOperations implements CipherOperations, InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(AESCipherOperations.class);

    @NotNull
    private Resource secretKeyFile = new ClassPathResource("key.aes256.dat");

    @NotNull
    private KeySize keySize = KeySize.AES256;

    @NotBlank
    private String blockCipherMode = "CBC";

    @NotBlank
    private String initVectorString = "31323334353630303030303030303030";

    private SecretKeySpec secretKey;

    public Resource getSecretKeyFile() {
        return secretKeyFile;
    }

    public void setSecretKeyFile(Resource secretKeyFile) {
        this.secretKeyFile = secretKeyFile;
    }

    public KeySize getKeySize() {
        return keySize;
    }

    public void setKeySize(KeySize keySize) {
        this.keySize = keySize;
    }

    public String getBlockCipherMode() {
        return blockCipherMode;
    }

    public void setBlockCipherMode(String blockCipherMode) {
        this.blockCipherMode = blockCipherMode;
    }

    public String getInitVectorString() {
        return initVectorString;
    }

    public void setInitVectorString(String initVectorString) {
        this.initVectorString = initVectorString;
    }

    @Override
    public byte[] encrypt(byte[] raw) throws CipherOperationException {
        LOGGER.trace("Encrypt raw bytes using aes cipher");
        try {
            String transformation = "AES/".concat(blockCipherMode).concat("/PKCS5PADDING");
            Cipher cipher = Cipher.getInstance(transformation);
            if (StringUtils.hasText(initVectorString)) {
                IvParameterSpec initVectorSpec = new IvParameterSpec(Hex.decodeHex(initVectorString.toCharArray()));
                cipher.init(Cipher.ENCRYPT_MODE, secretKey, initVectorSpec);
            } else {
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            }
            return cipher.doFinal(raw);
        } catch (Exception e) {
            throw new CipherOperationException(e);
        }
    }

    @Override
    public byte[] decrypt(byte[] encrypted) throws CipherOperationException {
        LOGGER.trace("Decrypt encrypted bytes using aes cipher");
        try {
            String transformation = "AES/".concat(blockCipherMode).concat("/PKCS5PADDING");
            Cipher cipher = Cipher.getInstance(transformation);
            if (StringUtils.hasText(initVectorString)) {
                IvParameterSpec initVector = new IvParameterSpec(Hex.decodeHex(initVectorString.toCharArray()));
                cipher.init(Cipher.DECRYPT_MODE, secretKey, initVector);
            } else {
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
            }
            return cipher.doFinal(encrypted);
        } catch (Exception e) {
            throw new CipherOperationException(e);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.debug("Validate key and initialize secret key");
        Assert.isTrue(secretKeyFile.exists(), "Given key file is not found");

        try (InputStreamReader streamReader = new InputStreamReader(secretKeyFile.getInputStream(), StandardCharsets.UTF_8)) {
            char[] buffer = new char[1024];
            StringBuilder encodedKeyBuilder = new StringBuilder();
            int readLength = 0;
            while ((readLength = streamReader.read(buffer, 0, buffer.length)) != -1) {
                encodedKeyBuilder.append(buffer, 0, readLength);
            }

            byte[] keyBytes = Base64.getDecoder().decode(encodedKeyBuilder.toString().getBytes(StandardCharsets.UTF_8));
            keyBytes = Arrays.copyOf(keyBytes, keySize.getValue() / 8);
            secretKey = new SecretKeySpec(keyBytes, "AES");
        }
    }

    /**
     * Define key size for AES chipper operation.
     */
    public static enum KeySize {
        AES128(128),
        AES192(192),
        AES256(256);

        private Integer value;

        private KeySize(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
    }
}