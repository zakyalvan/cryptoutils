package info.zakyalvan.crypto;

import java.util.Collections;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

/**
 * @author zakyalvan
 * @since 23 Aug 2017
 */
@SpringBootApplication
public class CryptoUtilsApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(CryptoUtilsApplication.class);

    public static void main(String[] args) {
        new SpringApplicationBuilder(CryptoUtilsApplication.class).properties(Collections.singletonMap("key.file", "key.dat")).run(args);
    }

    @Value("${alg.name:'aes'}")
    private String algorithmName;

    @Value("file:${key.file}")
    private Resource secretKeyFile;

    @Value("${block.cipher:}")
    private String blockCipherMode;

    @Value("${init.vector:}")
    private String initVector;

    @Value("${input.data:''}")
    private String data;

    @Value("${run.mode:'encrypt'}")
    private String runMode;

    private CipherOperations cipher;

    @Bean
    ApplicationRunner encryptRunner() {
        return args -> {
            if (!secretKeyFile.exists()) {
                throw new RuntimeException("Given key file (specified using 'key.file' parameter) is not exists");
            }

            if (!(algorithmName.equalsIgnoreCase("aes") || algorithmName.equalsIgnoreCase("3des"))) {
                throw new RuntimeException("Invalid algorithm name (specified using 'alg.name' parameter) given, only supporting either 'aes' or '3des'");
            }

            if (!(runMode.equalsIgnoreCase("encrypt") || runMode.equalsIgnoreCase("decrypt"))) {
                throw new RuntimeException("Run mode (specified using 'run.mode' parameter) must be either 'encrypt' or 'decrypt'");
            }

            if (StringUtils.hasText(initVector)) {
                try {
                    Hex.decodeHex(initVector.toCharArray());
                } catch (Exception e) {
                    throw new RuntimeException("Given init vector value (specified using 'init.vector' parameter) is not valid hex string", e);
                }
            }

            if (!StringUtils.hasText(data)) {
                throw new RuntimeException("No input data (specified using 'input.data' parameter) given, how come we can continue, huh?");
            }

            LOGGER.info("Using '{}' algorithm for '{}ing' given data '{}'", algorithmName, runMode, data);

            if (algorithmName.equalsIgnoreCase("aes")) {
                AESCipherOperations usedCipher = new AESCipherOperations();
                usedCipher.setSecretKeyFile(secretKeyFile);
                if(StringUtils.hasText(initVector)) {
                    usedCipher.setInitVectorString(initVector);
                }
                if(StringUtils.hasText(blockCipherMode)) {
                    usedCipher.setBlockCipherMode(blockCipherMode);
                }
                usedCipher.afterPropertiesSet();
                cipher = usedCipher;
            }
            if (algorithmName.equalsIgnoreCase("3des")) {
                TrippleDesCipherOperations usedCipher = new TrippleDesCipherOperations();
                usedCipher.setSecretKeyFile(secretKeyFile);
                if(StringUtils.hasText(initVector)) {
                    usedCipher.setInitVectorString(initVector);
                }
                if(StringUtils.hasText(blockCipherMode)) {
                    usedCipher.setBlockCipherMode(blockCipherMode);
                }
                usedCipher.afterPropertiesSet();
                cipher = usedCipher;
            }

            if (runMode.equalsIgnoreCase("encrypt")) {
                String result = cipher.encrypt(data, InputFunction.stringInput(), ReturnFunction.returnBase64String());
                LOGGER.info("{} encrypted to '{}'", data, result);
            } else {
                String result = cipher.decrypt(data, InputFunction.base64StringInput(), ReturnFunction.returnString());
                LOGGER.info("{} decrypted to '{}'", data, result);
            }
        };
    }
}
