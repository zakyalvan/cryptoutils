package info.zakyalvan.crypto;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Objects;
import java.util.function.Function;

/**
 * <p>Function for transforming input raw data (other than bytes array) 
 * to {@link CipherOperations} bytes array method parameter. See each inmpementation
 * inside.
 * 
 * @author zakyalvan
 * @since 14 Jan 2017
 * @param <I>
 */
public interface InputFunction<I> extends Function<I, byte[]> {
	public static StringInputFunction stringInput() {
		return new StringInputFunction();
	}
	public static StringInputFunction stringInput(Charset charset) {
		return new StringInputFunction(charset);
	}

	public static Base64StringInputFunction base64StringInput() {
		return new Base64StringInputFunction();
	}
	public static Base64StringInputFunction base64StringInput(Charset charset) {
		return new Base64StringInputFunction(charset);
	}

	public static CharArrayInputFunction charArrayInput() {
		return new CharArrayInputFunction();
	}
	public static CharArrayInputFunction charArrayInput(Charset charset) {
		return new CharArrayInputFunction(charset);
	}

	/**
	 * Handle string input.
	 */
	public static class StringInputFunction implements InputFunction<String> {
		private final Charset usedCharset;
		
		public StringInputFunction() {
			this(StandardCharsets.UTF_8);
		}
		public StringInputFunction(Charset usedCharset) {
			this.usedCharset = Objects.requireNonNull(usedCharset);
		}
		
		@Override
		public byte[] apply(String inputData) {
			return inputData.getBytes(usedCharset);
		}
	}
	
	/**
	 * Handle base64 encoded string.
	 */
	public static class Base64StringInputFunction implements InputFunction<String> {
		private final Charset usedCharset;
		
		public Base64StringInputFunction() {
			this(StandardCharsets.UTF_8);
		}
		public Base64StringInputFunction(Charset usedCharset) {
			this.usedCharset = Objects.requireNonNull(usedCharset);
		}
		
		@Override
		public byte[] apply(String inputData) {
			return Base64.getDecoder().decode(inputData.getBytes(usedCharset));
		}
	}
	
	/**
	 * Handle array character. 
	 */
	public static class CharArrayInputFunction implements InputFunction<char[]> {
		private final Charset usedCharset;
		
		public CharArrayInputFunction() {
			this(StandardCharsets.UTF_8);
		}
		public CharArrayInputFunction(Charset usedCharset) {
			this.usedCharset = Objects.requireNonNull(usedCharset);
		}
		
		@Override
		public byte[] apply(char[] inputData) {
			String inputString = String.valueOf(inputData);
			return inputString.getBytes(usedCharset);
		}	
	}
}
