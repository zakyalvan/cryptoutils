package info.zakyalvan.crypto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * <p>Contract for chiper operation, i.e. encryption and decryption.
 * 
 * <p>Please note, there are only two methods need to implement in this interface,
 * i.e {@link CipherOperations#encrypt(byte[])} and {@link CipherOperations#decrypt(byte[])};
 * 
 * @author zakyalvan
 * @since 13 Jan 2017
 */
public interface CipherOperations {
	/**
	 * Encrypt given bytes of data. You have to implement this according to required algorithm.
	 * 
	 * @param plain
	 * @return
	 * @throws CipherOperationException
	 */
	byte[] encrypt(@NotNull byte[] raw) throws CipherOperationException;
	
	/**
	 * 
	 * @param raw
	 * @param inputFunction
	 * @return
	 * @throws CipherOperationException
	 */
	default <I> byte[] encrypt(@NotNull I raw, InputFunction<I> inputFunction) throws CipherOperationException {
		return encrypt(applyInputFunction(raw, inputFunction));
	}

	/**
	 * 
	 * @param raw
	 * @param returnFunction
	 * @return
	 * @throws CipherOperationException
	 */
	default <R> R encrypt(@NotNull byte[] raw, ReturnFunction<R> returnFunction) throws CipherOperationException {
		return applyReturnFunction(encrypt(raw), returnFunction);
	}

	/**
	 * 
	 * @param raw
	 * @param inputFunction
	 * @param returnFunction
	 * @return
	 * @throws CipherOperationException
	 */
	default <I, R> R encrypt(@NotNull I raw, InputFunction<I> inputFunction, ReturnFunction<R> returnFunction)
			throws CipherOperationException {
		return applyReturnFunction(encrypt(applyInputFunction(raw, inputFunction)), returnFunction);
	}
	
	/**
	 * Decrypt given bytes of data. You have to implement this according to required algorithm.
	 * 
	 * @param encrypted
	 * @return
	 * @throws CipherOperationException
	 */
	byte[] decrypt(@NotEmpty byte[] encrypted) throws CipherOperationException;
	
	/**
	 * 
	 * @param encrypted
	 * @param inputFunction
	 * @return
	 * @throws CipherOperationException
	 */
	default <I> byte[] decrypt(I encrypted, InputFunction<I> inputFunction) throws CipherOperationException {
		return decrypt(applyInputFunction(encrypted, inputFunction));
	}

	/**
	 * 
	 * @param encrypted
	 * @param returnFunction
	 * @return
	 * @throws CipherOperationException
	 */
	default <R> R decrypt(byte[] encrypted, ReturnFunction<R> returnFunction) throws CipherOperationException {
		return applyReturnFunction(decrypt(encrypted), returnFunction);
	}

	/**
	 * 
	 * @param encrypted
	 * @param inputFunction
	 * @param returnFunction
	 * @return
	 * @throws CipherOperationException
	 */
	default <I, R> R decrypt(I encrypted, InputFunction<I> inputFunction, ReturnFunction<R> returnFunction)
			throws CipherOperationException {
		return applyReturnFunction(decrypt(applyInputFunction(encrypted, inputFunction)), returnFunction);
	}
	
	/**
	 * 
	 * @param inputObject
	 * @param inputFunction
	 * @return
	 */
	default <I> byte[] applyInputFunction(I inputObject, InputFunction<I> inputFunction) {
		byte[] inputBytes = null;
		try {
			inputBytes = inputFunction.apply(inputObject);
		} catch(Exception e) {
			throw new CipherOperationException("Exception on applying given input function. See stack trace", e);
		}
		return inputBytes;
	}
	
	/**
	 * 
	 * @param returnBytes
	 * @param returnFunction
	 * @return
	 */
	default <R> R applyReturnFunction(byte[] returnBytes, ReturnFunction<R> returnFunction) {
		R returnObject = null;
		try {
			returnObject = returnFunction.apply(returnBytes);
		} catch(Exception e) {
			throw new CipherOperationException("Exception on applying given return function. See stack trace", e);
		}
		return returnObject;
	}
}
