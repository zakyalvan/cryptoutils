package info.zakyalvan.crypto;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Objects;
import java.util.function.Function;

/**
 * Function for transforming returned bytes of data from {@link CipherOperations}
 * methods to other than bytes array data type.
 * 
 * @author zakyalvan
 * @since 14 Jan 2017
 * @param <R>
 */
public interface ReturnFunction<R> extends Function<byte[], R> {
	public static StringReturnFunction returnString() {
		return new StringReturnFunction();
	}
	public static StringReturnFunction returnString(Charset charset) {
		return new StringReturnFunction(charset);
	}
	public static Base64StringReturnFunction returnBase64String() {
		return new Base64StringReturnFunction();
	}
	public static Base64StringReturnFunction returnBase64String(Charset charset) {
		return new Base64StringReturnFunction(charset);
	}
	public static CharArrayReturnFunction returnCharArray() {
		return new CharArrayReturnFunction();
	}
	public static CharArrayReturnFunction returnCharArray(Charset charset) {
		return new CharArrayReturnFunction(charset);
	}

	/**
	 * Returning string.
	 */
	public static class StringReturnFunction implements ReturnFunction<String> {
		private final Charset usedCharset;
		
		public StringReturnFunction() {
			this(StandardCharsets.UTF_8);
		}
		public StringReturnFunction(Charset usedCharset) {
			this.usedCharset = Objects.requireNonNull(usedCharset);
		}
		
		@Override
		public String apply(byte[] returnBytes) {
			return new String(returnBytes, usedCharset);
		}
	}
	
	/**
	 * Returning Base64 encoded string.
	 */
	public static class Base64StringReturnFunction implements ReturnFunction<String> {
		private final Charset usedCharset;
		
		public Base64StringReturnFunction() {
			this(StandardCharsets.UTF_8);
		}
		public Base64StringReturnFunction(Charset usedCharset) {
			this.usedCharset = Objects.requireNonNull(usedCharset);
		}
		
		@Override
		public String apply(byte[] returnBytes) {
			return new String(Base64.getEncoder().encode(returnBytes), usedCharset);
		}	
	}
	
	/**
	 * Returning char array.
	 */
	public static class CharArrayReturnFunction implements ReturnFunction<char[]> {
		private final Charset usedCharset;
		
		public CharArrayReturnFunction() {
			this(StandardCharsets.UTF_8);
		}
		public CharArrayReturnFunction(Charset usedCharset) {
			this.usedCharset = Objects.requireNonNull(usedCharset);
		}
		
		@Override
		public char[] apply(byte[] returnBytes) {
			return new String(returnBytes, usedCharset).toCharArray();
		}		
	}
}
